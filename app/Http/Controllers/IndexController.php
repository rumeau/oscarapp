<?php

namespace App\Http\Controllers;

use App\Http\Requests\Start as StartRequest;
use App\Models\Client;
use App\Models\Order;
use App\Services\StartProjectService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function start(StartRequest $request, StartProjectService $startProjectService)
    {
        try {
            $project = $startProjectService->start($request);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return 'Exito';
    }

    public function order(Request $request, $hash = null)
    {
        $order = Order::where(\DB::raw('MD5(CONCAT(`order`, \'_\', `project_id`))'), '=', $hash)->firstOrFail();

        return view('index.order', [
            'order' => $order,
        ]);
    }
}
