<?php

namespace App\Http\Controllers\Admin;

use App\Mail\QuoteCreated;
use App\Models\Order;
use App\Models\Project;
use App\Models\ProjectActivity;
use App\Models\ProjectStates;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use Mail;

class ProjectController extends Controller
{
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function index(Request $request)
    {
        $query = Project::query()->with('client');
        $query->orderBy($request->query('order', 'created_at'), $request->query('sort', 'DESC'));

        return view('admin.projects.index', [
            'projects' => $query->paginate($request->query('per_page', 20)),
        ]);
    }

    /**
     * 
     * @return type
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * 
     * @return type
     */
    public function store()
    {
        return abort(404);
    }

    /**
     * 
     * @param Request $request
     * @param Project $project
     * @return type
     */
    public function edit(Request $request, Project $project)
    {
        return view('admin.projects.edit', [
            'project' => $project,
            'attachments' => $project->attachments(),
        ]);
    }

    /**
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Project $project)
    {
        $this->validate($request, [
            'comment' => 'sometimes',
        ]);

        $project->comment = $request->input('comment', '');
        $project->save();

        ProjectActivity::create([
            'project_id' => $project->id,
            'description' => trans('activity.project_activity.project_updated'),
        ]);

        Alert::success('Los cambios han sido guardados!')->autoclose(3000);

        return redirect()->route('admin.projects.edit', ['project' => $project]);
    }

    /**
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function quote(Request $request, Project $project)
    {
        return view('admin.projects.quote', ['project' => $project]);
    }

    /**
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function quoteStore(Request $request, Project $project)
    {
        $this->validate($request, [
            'subject' => 'required',
            'body' => 'required',
            'amount' => 'required',
        ]);

        $order = Order::create([
            'project_id' => $project->id,
            'order' => str_random(20),
            'transaction_reference' => null,
            'amount' => $request->input('amount'),
            'currency' => 'CLP',
            'status' => 0,
            'order_data' => [],
            'response_data' => [],
            'gateway' => null,
            'payment_date' => null,
            'extra' => ['currency' => 'CLP'],
        ]);

        Mail::to($project->client)->send(new QuoteCreated($project, $request, $order));

        $project->status = ProjectStates::STATE_QUOTE_GENERATED;
        $project->save();

        ProjectActivity::create([
            'project_id' => $project->id,
            'description' => trans('activity.project_activity.quote_sent', ['date' => Carbon::now()->format('d/m/Y H:i:s')]),
        ]);

        Alert::success('La cotizacion ha sido enviada')->autoclose(3000);

        return redirect()->route('admin.projects.edit', ['project' => $project]);
    }
}
