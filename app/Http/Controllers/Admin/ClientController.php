<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $query = Client::query();
        $query->orderBy($request->query('order', 'created_at'), $request->query('sort', 'DESC'));

        return view('admin.clients.index', [
            'clients' => $query->paginate($request->query('per_page', 20)),
        ]);
    }
}
