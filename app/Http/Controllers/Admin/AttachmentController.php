<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Project;
use App\Models\ProjectActivity;
use App\Services\AddAttachmentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttachmentController extends Controller
{
    const ATTACHMENT_TYPE_DOC = 'DOC';

    /**
     * @param Request $request
     * @param Project $project
     * @param int $clientProvided
     * @return mixed
     */
    public function all(Request $request, Project $project, $clientProvided = 0)
    {
        return $project->attachments()->where('user_provided', (int) $clientProvided)->get();
    }

    /**
     * @param Request $request
     * @param Project $project
     * @param int $clientProvided
     * @param AddAttachmentService $addAttachmentService
     * @return Attachment|array
     */
    public function upload(Request $request, Project $project, $clientProvided = 0, AddAttachmentService $addAttachmentService)
    {
        if ($request->file('file', null) !== null) {
            $attachmentType = AttachmentType::whereCode(self::ATTACHMENT_TYPE_DOC)->firstOrFail();
            $addAttachmentService->setProject($project);
            $attachment = $addAttachmentService->addAttachment($attachmentType, $request->file('file'));

            $attachment->user_provided = (int) $clientProvided;
            $attachment->save();

            ProjectActivity::create([
                'project_id' => $project->id,
                'description' => trans('activity.project_activity.new_attachment', ['filename' => $attachment->filename]),
            ]);

            return $attachment;
        }

        return [];
    }

    /**
     * @param Request $request
     * @param Attachment $attachment
     * @return Attachment
     */
    public function update(Request $request, Attachment $attachment)
    {
        $attachment->attachment_type_id = (int) $request->input('attachment_type_id', 1);
        $attachment->save();

        ProjectActivity::create([
            'project_id' => $attachment->project_id,
            'description' => trans('activity.project_activity.attachment_type_updated', ['filename' => $attachment->filename, 'attachmentType' => $attachment->attachmentType->name]),
        ]);

        return $attachment;
    }

    /**
     * @param Request $request
     * @param Attachment $attachment
     * @return array
     */
    public function delete(Request $request, Attachment $attachment)
    {
        \Storage::delete($attachment->path);
        $attachment->delete();

        ProjectActivity::create([
            'project_id' => $attachment->project_id,
            'description' => trans('activity.project_activity.attachment_deleted', ['filename' => $attachment->filename]),
        ]);

        return [];
    }

    /**
     * @param Request $request
     * @param Attachment $attachment
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Request $request, Attachment $attachment)
    {
        return response()->download(storage_path('app/' . $attachment->path), $attachment->filename);
    }
}
