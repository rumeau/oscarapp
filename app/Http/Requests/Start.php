<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Start extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'is_company' => 'required|in:1,2',
            'business' => 'required_if:is_company,2',
            'file' => 'sometimes|file|mimes:zip,rar,dwg',
        ];
    }
}
