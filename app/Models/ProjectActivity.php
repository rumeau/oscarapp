<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProjectActivity
 *
 * @property-read \App\Models\Project $project
 * @mixin \Eloquent
 * @property int $id
 * @property int $project_id
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectActivity whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectActivity whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectActivity whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectActivity whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectActivity whereUpdatedAt($value)
 */
class ProjectActivity extends Model
{
    protected $table = 'project_activity';

    protected $fillable = [
        'project_id',
        'description',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
