<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AttachmentType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AttachmentType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AttachmentType whereName($value)
 * @property string $code
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AttachmentType whereCode($value)
 */
class AttachmentType extends Model
{
    public $timestamps = false;
}
