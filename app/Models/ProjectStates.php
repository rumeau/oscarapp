<?php

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectStates
 *
 * @package App\Models
 * @mixin \Eloquent
 * @property int $id
 * @property string $code
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectStates whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectStates whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProjectStates whereName($value)
 */
class ProjectStates extends Model
{
    const CACHE_KEY = __CLASS__ . '_cache';

    const STATE_PROJECT_CREATED = 'IC';
    const STATE_QUOTE_GENERATED = 'CG';

    public $timestamps = false;

    /**
     * @param null $state
     * @return mixed|null
     */
    public static function states($state = null)
    {
        if (!Cache::has(self::CACHE_KEY)) {
            $instance = new self();
            $states = $instance->all()->keyBy('code');
            Cache::put(self::CACHE_KEY, $states, 60);
        } else {
            $states = Cache::get(self::CACHE_KEY);
        }

        if ($state !== null) {
            if (isset($states[$state])) {
                return $states[$state];
            }

            return null;
        }

        return $states;
    }
}
