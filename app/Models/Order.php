<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $project_id
 * @property string $order
 * @property string $transaction_reference
 * @property string $amount
 * @property int $status
 * @property string $currency
 * @property array $order_data
 * @property array $response_data
 * @property string $gateway
 * @property \Carbon\Carbon $payment_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property array $extra
 * @property-read string $hash
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereTransactionReference($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereOrderData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereResponseData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereGateway($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order wherePaymentDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereExtra($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    public $fillable = [
        'project_id',
        'order',
        'transaction_reference',
        'amount',
        'currency',
        'status',
        'order_data',
        'response_data',
        'gateway',
        'payment_date',
        'extra',
    ];

    protected $dates = [
        'payment_date',
    ];

    protected $casts = [
        'order_data' => 'array',
        'response_data' => 'array',
        'extra' => 'array',
    ];

    /**
     * @return string
     */
    public function getHashAttribute()
    {
        return md5($this->attributes['order'] . '_' . $this->attributes['project_id']);
    }
}
