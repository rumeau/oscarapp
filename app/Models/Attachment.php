<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Attachment
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $project_id
 * @property int $attachment_type_id
 * @property int $user_provided
 * @property mixed $metadata
 * @property string $path
 * @property string $filename
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereAttachmentTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereUserProvided($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereMetadata($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Attachment whereUpdatedAt($value)
 * @property-read \App\Models\Project $project
 * @property-read \App\Models\AttachmentType $attachmentType
 */
class Attachment extends Model
{
    protected $fillable = [
        'project_id',
        'attachment_type_id',
        'user_provided',
        'metadata',
        'path',
        'filename',
    ];

    protected $casts = [
        'metadata' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attachmentType()
    {
        return $this->belongsTo('App\Models\AttachmentType');
    }
}
