<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Project
 *
 * @property-read string $state_color
 * @mixin \Eloquent
 * @property int $id
 * @property string $status
 * @property string $comment
 * @property int $client_id
 * @property int $attachment_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereAttachmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project whereUpdatedAt($value)
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Attachment $attachment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attachment[] $attachments
 * @property-read mixed $client_attachments
 * @property-read mixed $admin_attachments
 */
class Project extends Model
{
    protected $fillable = [
        'status',
        'comment',
        'client_id',
        'attachment_id',
    ];

    protected $appends = [
        'state_color',
    ];

    /**
     * @return string
     */
    public function getStateColorAttribute()
    {
        switch ($this->attributes['status']) {
            case 'CG':
                $color = 'warning';
                break;

            case 'CA':
                $color = 'success';
                break;

            case 'R':
                $color = 'danger';
                break;

            case 'F':
                $color = 'active';
                break;

            default:
            case 'IC':
                $color = 'info';
                break;
        }

        return  $color;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attachment()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany('App\Models\Attachment');
    }

    /**
     * @return mixed
     */
    public function getClientAttachmentsAttribute()
    {
        return $this->attachments()->where('user_provided', 1)->get();
    }

    /**
     * @return mixed
     */
    public function getAdminAttachmentsAttribute()
    {
        return $this->attachments()->where('user_provided', 0)->get();
    }
}
