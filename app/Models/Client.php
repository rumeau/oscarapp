<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Client
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $client_type
 * @property string $name
 * @property string $email
 * @property string $business_contact
 * @property string $address
 * @property string $phone
 * @property string $observation
 * @property string $country
 * @property string $city
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereClientType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereBusinessContact($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereObservation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
class Client extends Model
{
    use Notifiable;

    const CLIENT_TYPE_PERSON = 1;
    const CLIENT_TYPE_COMPANY = 2;

    protected $fillable = [
        'client_type',
        'name',
        'email',
        'business_contact',
        'address',
        'phone',
        'observation',
        'country',
        'city',
    ];

    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }
}
