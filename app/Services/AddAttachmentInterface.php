<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 19/01/2017
 * Time: 12:08
 */

namespace App\Services;


use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Project;
use Illuminate\Http\UploadedFile;

interface AddAttachmentInterface
{
    public function setProject(Project $project);

    /**
     * @param AttachmentType $type
     * @param UploadedFile $file
     * @return Attachment
     */
    public function addAttachment(AttachmentType $type, UploadedFile $file);
}