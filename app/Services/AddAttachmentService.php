<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 19/01/2017
 * Time: 12:05
 */

namespace App\Services;


use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Project;
use Illuminate\Http\UploadedFile;

class AddAttachmentService implements AddAttachmentInterface
{
    /**
     * @var string
     */
    protected $storagePath;

    /**
     * @var Project
     */
    protected $project;

    /**
     * AddAttachmentService constructor.
     */
    public function __construct()
    {
        $this->storagePath = 'projects';
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @param AttachmentType $attachmentType
     * @param UploadedFile $file
     * @return Attachment
     * @throws \Exception
     */
    public function addAttachment(AttachmentType $attachmentType, UploadedFile $file)
    {
        if (!$this->project instanceof Project) {
            throw new \Exception('Project must be defined in the AddAttachmentService with setProject() before storing the attachment');
        }

        $originalName = $file->getClientOriginalName();
        $path = $file->store('projects');

        $attachment = Attachment::create([
            'project_id' => $this->project->id,
            'attachment_type_id' => $attachmentType->id,
            'user_provided' => 0,
            'metadata' => [
                'name' => $file->getClientOriginalName(),
                'ext' => $file->getClientOriginalExtension(),
                'size' => $file->getClientSize(),
                'type' => $file->getType(),
                'mime' => $file->getClientMimeType(),
            ],
            'path' => $path,
            'filename' => $originalName,
        ]);

        return $attachment;
    }
}