<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 19/01/2017
 * Time: 12:01
 */

namespace App\Services;


use App\Events;
use App\Models\AttachmentType;
use App\Models\Client;
use App\Models\Project;
use App\Models\ProjectStates;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;

class StartProjectService
{
    const START_PROJECT_DEFAULT_STATUS = 'IC';
    const START_PROJECT_ATTACHMENT_TYPE = 'PLA';

    protected $mailer;
    
    protected $request;

    /**
     * @var AddAttachmentInterface
     */
    protected $attachmentService;
    
    public function __construct(Request $request, Mailer $mailer, AddAttachmentService $attachmentService)
    {
        $this->request = $request;
        $this->mailer = $mailer;
        
        $this->setAddAttachmentService($attachmentService);
    }

    /**
     * @param AddAttachmentInterface $attachmentService
     */
    public function setAddAttachmentService(AddAttachmentInterface $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function start(Request $request)
    {
        $client = $this->createClient($request);
        $project = $this->createProject($client, $request);

        event(new Events\ProjectStarted($client, $project));
    }

    /**
     * @param Request $request
     * @return Client
     */
    protected function createClient(Request $request)
    {
        return Client::create([
            'client_type' => $request->input('is_company', 1),
            'name' => $request->input('is_company', 1) == 2 ? $request->input('business', '') : $request->input('name', ''),
            'email' => $request->input('email', ''),
            'business_contact' => $request->input('is_company', 1) == 2 ? $request->input('name', '') : '',
            'address' => '',
            'phone' => '',
            'observation' => '',
            'country' => 'CL',
            'city',
        ]);
    }

    protected function createProject(Client $client, Request $request)
    {
        $status = ProjectStates::whereCode(self::START_PROJECT_DEFAULT_STATUS)->firstOrFail();
        $project = Project::create([
            'status' => $status->code,
            'comment' => '-- Proyecto generado por sistema --',
            'client_id' => $client->id,
        ]);

        if ($request->file('file', null) !== null) {
            $attachmentType = AttachmentType::whereCode(self::START_PROJECT_ATTACHMENT_TYPE)->firstOrFail();
            $this->attachmentService->setProject($project);
            $attachment = $this->attachmentService->addAttachment($attachmentType, $request->file('file'));

            $attachment->user_provided = 1;
            $attachment->save();

            $project->attachment()->associate($attachment);
            $project->save();
        }

        return $project;
    }
}