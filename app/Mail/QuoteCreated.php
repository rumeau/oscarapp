<?php

namespace App\Mail;

use App\Models\Attachment;
use App\Models\Order;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuoteCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Project
     */
    public $project;

    /**
     * @var mixed
     */
    public $subject;

    /**
     * @var Request
     */
    public  $body;

    /**
     * @var
     */
    public $attachmentFiles;

    /**
     * @var Order
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @param Project $project
     * @param Request $request
     * @param Order $order
     */
    public function __construct(Project $project, Request $request, Order $order)
    {
        $this->project = $project;
        $this->subject = $request->get('subject', '');
        $this->body = $request->get('body', '');
        $this->attachmentFiles = Attachment::whereIn('id', $request->get('files'))->get();
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $msg = $this->view('emails.quote');
        foreach ($this->attachmentFiles as $attachment) {
            $msg->attach(storage_path('app/' . $attachment->path), [
                'as' => $attachment->filename,
                'mime' => $attachment->metadata['mime'],
            ]);
        }
        $msg->subject($this->subject);

        return $msg;
    }
}
