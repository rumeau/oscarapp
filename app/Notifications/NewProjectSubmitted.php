<?php

namespace App\Notifications;

use App\Models\Admin;
use App\Models\Client;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewProjectSubmitted extends Notification
{
    use Queueable;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Project
     */
    private $project;

    /**
     * Create a new notification instance.
     *
     * @param Client $client
     * @param Project $project
     */
    public function __construct(Client $client, Project $project)
    {
        //
        $this->client = $client;
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed|Admin  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(trans('notification.new_project_submitted.subject'))
                    ->greeting(trans('notification.new_project_submitted.greeting', ['name' => $notifiable->name]))
                    ->line(trans('notification.new_project_submitted.line_1'))
                    ->line(trans('notification.new_project_submitted.att_1', ['name' => $this->client->client_type === Client::CLIENT_TYPE_COMPANY ? $this->client->business_contact : $this->client->name]))
                    ->line(trans('notification.new_project_submitted.att_2', ['email' => $this->client->email]))
                    ->line(trans('notification.new_project_submitted.att_2', ['company' => $this->client->client_type === Client::CLIENT_TYPE_COMPANY ? $this->client->name : '']))
                    ->action(trans('notification.new_project_submitted.action_1'), route('admin.projects.edit', ['project' => $this->project->id]))
                    ->line(trans('notification.new_project_submitted.line_2'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
