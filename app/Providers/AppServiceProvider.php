<?php

namespace App\Providers;

use App\Services\AddAttachmentService;
use App\Services\SendQuoteService;
use App\Services\StartProjectService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias(StartProjectService::class, 'project.start');
        $this->app->alias(AddAttachmentService::class, 'attachment.upload');
    }
}
