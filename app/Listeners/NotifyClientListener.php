<?php

namespace App\Listeners;

use App\Models\ProjectActivity;
use App\Notifications;
use App\Events\ProjectStarted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyClientListener
{
    /**
     * Handle the event.
     *
     * @param  ProjectStarted  $event
     * @return void
     */
    public function handle(ProjectStarted $event)
    {
        $event->getClient()->notify(new Notifications\ProjectStarted($event->getProject()));

        ProjectActivity::create([
            'project_id' => $event->getProject()->id,
            'description' => trans('activity.project_activity.project_created'),
        ]);
    }
}
