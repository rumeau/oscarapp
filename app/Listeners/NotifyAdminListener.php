<?php

namespace App\Listeners;

use App\Notifications;
use App\Events\ProjectStarted;
use App\Models\Admin;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminListener
{
    /**
     * Handle the event.
     *
     * @param  ProjectStarted  $event
     * @return void
     */
    public function handle(ProjectStarted $event)
    {
        try {
            $admin = Admin::whereEmail(env('ADMIN_EMAIL', ''))->first();

            $admin->notify(new Notifications\NewProjectSubmitted($event->getClient(), $event->getProject()));
        } catch (\Exception $e) {
            return;
        }
    }
}
