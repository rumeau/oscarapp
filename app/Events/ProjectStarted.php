<?php

namespace App\Events;

use App\Models\Client;
use App\Models\Project;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProjectStarted
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Project
     */
    private $project;

    /**
     * Create a new event instance.
     *
     * @param Client $client
     * @param Project $project
     */
    public function __construct(Client $client, Project $project)
    {
        $this->client = $client;
        $this->project = $project;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
