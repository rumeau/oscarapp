<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');
Route::post('/start', ['as' => 'start', 'uses' => 'IndexController@start']);
Route::get('/order/{hash}', ['as' => 'order', 'uses' => 'IndexController@order']);

Route::group(['prefix' => 'platform'], function () {
    Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'auth'], function () {
        Route::get('/', 'IndexController@dashboard');

        crudRoutes('projects', 'project', 'ProjectController');
        crudRoutes('clients', 'client', 'ClientController');

        Route::get('attachments/{project}/{client}', 'AttachmentController@all');
        Route::post('attachment/{project}/{client}', 'AttachmentController@upload');
        Route::put('attachment/{attachment}', 'AttachmentController@update');
        Route::delete('attachment/{attachment}', 'AttachmentController@delete');
        Route::get('attachment/download/{attachment}', 'AttachmentController@download');

        Route::get('project/{project}/quote', ['as' => 'projects.quote', 'uses' => 'ProjectController@quote']);
        Route::post('project/{project}/quote', ['as' => 'projects.quote.store', 'uses' => 'ProjectController@quoteStore']);
        Route::get('project/{project}/job', ['as' => 'projects.job', 'uses' => 'ProjectController@job']);
        Route::post('project/{project}/job', ['as' => 'projects.job.store', 'uses' => 'ProjectController@jobStore']);
    });

    Auth::routes();
});

function crudRoutes($resource, $resourceSingular, $controllerName) {
    Route::get('/' . $resource, ['as' => $resource, 'uses' => $controllerName . '@index']);
    Route::get('/' . $resource . '/create', ['as' => $resource . '.create', 'uses' => $controllerName . '@create']);
    Route::post('/' . $resource . '/create', ['as' => $resource . '.store', 'uses' => $controllerName . '@store']);
    Route::get('/' . $resource . '/edit/{' . $resourceSingular . '}', ['as' => $resource . '.edit', 'uses' => $controllerName . '@edit']);
    Route::post('/' . $resource . '/edit/{' . $resourceSingular . '}', ['as' => $resource . '.update', 'uses' => $controllerName . '@update']);
    Route::post('/' . $resource . '/delete/{' . $resourceSingular . '}', ['as' => $resource . '.delete', 'uses' => $controllerName . '@delete']);
}
