<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->string('order');
            $table->string('transaction_reference');
            $table->string('amount');
            $table->integer('status');
            $table->string('currency');
            $table->text('order_data')->nullable();;
            $table->text('response_data')->nullable();
            $table->string('gateway');
            $table->timestamp('payment_date')->nullable();
            $table->timestamps();
            $table->text('extra')->nullable();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
