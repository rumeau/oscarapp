<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
        });

        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('attachment_type_id')->unsigned()->nullable();
            $table->integer('user_provided')->nullable();
            $table->json('metadata')->nullable();
            $table->string('path');
            $table->string('filename');
            $table->timestamps();
        });

        Schema::table('attachments', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('attachment_type_id')->references('id')->on('attachment_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachments');
        Schema::drop('attachment_types');
    }
}
