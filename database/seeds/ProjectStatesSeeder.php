<?php

use Illuminate\Database\Seeder;

class ProjectStatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\ProjectStates::create([
            'code' => 'IC',
            'name' => 'Ingresado para cotizacion',
        ]);

        \App\Models\ProjectStates::create([
            'code' => 'CG',
            'name' => 'Cotizacion generada',
        ]);

        \App\Models\ProjectStates::create([
            'code' => 'CA',
            'name' => 'Cotizacion aprobada',
        ]);

        \App\Models\ProjectStates::create([
            'code' => 'R',
            'name' => 'Cotizacion Rechazada',
        ]);

        \App\Models\ProjectStates::create([
            'code' => 'F',
            'name' => 'Projecto Finalizado',
        ]);
    }
}
