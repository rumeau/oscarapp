<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::create([
            'name' => 'Jean Rumeau',
            'email' => 'rumeau@gmail.com',
            'password' => bcrypt('rumeau'),
        ]);
    }
}
