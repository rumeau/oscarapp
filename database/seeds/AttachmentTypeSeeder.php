<?php

use Illuminate\Database\Seeder;

class AttachmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\AttachmentType::create([
            'name' => 'Documento',
            'code' => 'DOC',
        ]);

        \App\Models\AttachmentType::create([
            'name' => 'Plano',
            'code' => 'PLA',
        ]);

        \App\Models\AttachmentType::create([
            'name' => 'Memoria de Calculo',
            'code' => 'MC',
        ]);

        \App\Models\AttachmentType::create([
            'name' => 'Especificaciones Tecnicas',
            'code' => 'ET',
        ]);

        \App\Models\AttachmentType::create([
            'name' => 'Planos Estructurales',
            'code' => 'PLE',
        ]);

        \App\Models\AttachmentType::create([
            'name' => 'Presupuesto',
            'code' => 'PR'
        ]);
    }
}
