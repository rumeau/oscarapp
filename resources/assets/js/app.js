
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */
import './bootstrap';
import VueInternationalization from 'vue-i18n';
import Locales from './vue-i18n-locales.generated.js';

Vue.use(VueInternationalization);

Vue.config.lang = 'es';

Object.keys(Locales).forEach(function (lang) {
    Vue.locale(lang, Locales[lang])
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('start', require('./components/Start.vue'));
Vue.component('attachmentlist', require('./components/AttachmentList.vue'));

const app = new Vue({
    el: '#app'
});
