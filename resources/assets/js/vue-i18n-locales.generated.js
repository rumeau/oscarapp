export default {
    "en": {
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "notification": {
            "project_started": {
                "greeting": "Hola!",
                "line_1": "Hemos recibido su solicitud",
                "line_2": "Uno de nuestros administradores se pondrá en contacto con usted a la brevedad"
            },
            "regards": "Atentamente",
            "problem_link": "If you’re having trouble clicking the \"{{ $actionText }}\" button,\r\n                                                            copy and paste the URL below into your web browser{}"
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field is required.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type{} {values}.",
            "mimetypes": "The {attribute} must be a file of type{} {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    },
    "es": {
        "activity": {
            "project_activity": {
                "project_started": "Proyecto ingresado en sistema"
            }
        },
        "admin": {
            "menu": {
                "projects": "Proyectos",
                "clients": "Clientes"
            },
            "dashboard": "Dashboard",
            "welcome_msg": "Bienvenido a su panel de administracion",
            "projects": {
                "title": "Projectos",
                "table": {
                    "client_name": "Cliente",
                    "created_at": "Fecha de Ingreso",
                    "status": "Estado",
                    "options": "Opciones",
                    "no_results_msg": "No se han encontrado resultados"
                }
            },
            "clients": {
                "title": "Clientes",
                "table": {
                    "type": "Tipo de Cliente",
                    "client_name": "Cliente",
                    "contact_name": "Contacto",
                    "email": "Email",
                    "created_at": "Fecha de Ingreso",
                    "options": "Opciones",
                    "no_results_msg": "No se han encontrado resultados"
                }
            }
        },
        "form": {
            "your_name": "Su Nombre",
            "your_email": "Su E-mail",
            "your_company_name": "Nombre de su empresa",
            "provide_your_attachment_msg": "Proporcione sus planos en formato DWG para entregarle un servicio mas rápido y eficiente",
            "create_project_submit": "Enviar"
        },
        "menu": {
            "home": "Inicio",
            "begin": "Solicita un Presupuesto",
            "comments": "Comentarios"
        },
        "notification": {
            "project_started": {
                "subject": "Su solicitud ha sido ingresada!",
                "greeting": "Hola, {name}!",
                "line_1": "Hemos recibido su solicitud",
                "line_2": "Uno de nuestros administradores se pondrá en contacto con usted a la brevedad"
            },
            "new_project_submitted": {
                "subject": "Nueva solicitud de presupuesto ingresada!",
                "greeting": "Hola, {name}!",
                "line_1": "Un nuevo proyecto ha sido ingresado en la plataforma para presupuesto, a continuacion estan los detalles{}",
                "att_1": "Nombre{} ",
                "att_2": "Email{} ",
                "att_3": "Empresa{} ",
                "action_1": "Ver en la Plataforma",
                "line_2": "El cliente ha sido notificado de que el ingreso ha sido exitoso."
            },
            "whoops": "Oops",
            "hello": "Hola!",
            "regards": "Atentamente",
            "problem_link": "Si tiene problemas al hacer click en el botón \"{actionText}\", copie y pegue la URL a continuacion en su navegador{}"
        }
    }
}
