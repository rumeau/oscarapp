@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">

            <div class="page-header">
                <h1>{{ trans('admin.projects.title') }} - <small>{{ $project->created_at->format('d/m/Y H:i') }}</small></h1>
            </div>

            {!! Form::model($project, ['method' => 'post', 'class' => 'form-horizontal', 'url' => route('admin.projects.update', ['project' => $project])]) !!}

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="fa {{ $project->client->client_type === \App\Models\Client::CLIENT_TYPE_PERSON ? 'fa-user' : 'fa-building' }}"></span>
                            &nbsp;{{ $project->client->name }}
                            @if ($project->status === \App\Models\ProjectStates::STATE_QUOTE_GENERATED)
                                <span class="label label-success">{{ trans('admin.projects.quote.quote_already_exists') }}</span>
                            @endif
                        </div>

                        <div class="panel-body">
                            <dl class="">
                                @if ($project->client->client_type === \App\Models\Client::CLIENT_TYPE_COMPANY)
                                    <dt>{{ trans('admin.projects.contact_name') }}</dt>
                                    <dd>{{ $project->client->contact_name }}</dd>
                                @endif

                                <dt>{{ trans('admin.projects.email') }}</dt>
                                <dd>{{ $project->client->email }}</dd>
                            </dl>

                            <div class="">
                                <label for="comment" class="control-label">{{ trans('admin.projects.comment') }}</label>
                                {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 2]) !!}
                            </div>

                            <hr >
                            <div class="">
                                <button type="submit" class="btn btn-primary">{{ trans('form.save_comment_button') }}</button>
                                <a href="{{ route('admin.projects.quote', ['project' => $project]) }}" class="btn btn-info">{{ trans('form.send_quote_button') }}</a>
                                <a href="{{ route('admin.projects.job', ['project' => $project]) }}" class="btn btn-info">{{ trans('form.send_job_button') }}</a>
                            </div>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}

            <div class="col-md-12">
                <div class="page-header">
                    <h1>{{ trans('admin.projects.project_documents') }}</h1>
                </div>

                <div class="col-md-6">
                    <attachmentlist :client="1" :project="{{ $project->id }}" title="{{ trans('admin.projects.client_docs') }}" :attachment-types="{{ \App\Models\AttachmentType::get(['id', 'name']) }}"></attachmentlist>
                </div>

                <div class="col-md-6">
                    <attachmentlist :client="0" :project="{{ $project->id }}" title="{{ trans('admin.projects.admin_docs') }}" :attachment-types="{{ \App\Models\AttachmentType::get(['id', 'name']) }}"></attachmentlist>
                </div>
            </div>
        </div>
    </div>
@endsection
