@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">

            <div class="page-header">
                <h1>{{ trans('admin.projects.quote.title') }} - <small>{{ $project->created_at->format('d/m/Y H:i') }}</small></h1>
            </div>

            {!! Form::model($project, ['method' => 'post', 'class' => 'form-horizontal', 'url' => route('admin.projects.quote.store', ['project' => $project])]) !!}

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="fa {{ $project->client->client_type === \App\Models\Client::CLIENT_TYPE_PERSON ? 'fa-user' : 'fa-building' }}"></span>
                            &nbsp;{{ $project->client->name }}
                            @if ($project->status === \App\Models\ProjectStates::STATE_QUOTE_GENERATED)
                                <span class="label label-success">{{ trans('admin.projects.quote.quote_already_exists') }}</span>
                            @endif
                        </div>

                        <div class="panel-body">
                            <dl class="">
                                @if ($project->client->client_type === \App\Models\Client::CLIENT_TYPE_COMPANY)
                                    <div class="form-group">
                                        <label class="control-label col-md-3">{{ trans('admin.projects.contact_name') }}</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static">{{ $project->client->contact_name }}</p>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ trans('admin.projects.email') }}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $project->client->email }}</p>
                                    </div>
                                </div>
                            </dl>

                            <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                                <label for="comment" class="control-label col-md-3">{{ trans('form.quote.subject') }}</label>
                                <div class="col-md-5">
                                    {!! Form::text('subject', null, ['class' => 'form-control', 'rows' => 2]) !!}
                                </div>
                                @if ($errors->has('subject'))
                                    {!! $errors->first('subject', '<p class="col-md-offset-3 col-md-9">:message</p>') !!}
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                                <label for="comment" class="control-label col-md-3">{{ trans('form.quote.body') }}</label>
                                <div class="col-md-9">
                                    <textarea id="summernote" name="body"></textarea>
                                </div>
                                @if ($errors->has('body'))
                                    {!! $errors->first('body', '<p class="col-md-offset-3 col-md-9 text-error">:message</p>') !!}
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="comment" class="control-label col-md-3">{{ trans('form.quote.select_files') }}</label>
                                <div class="col-md-9">
                                    <h3>{{ trans('admin.projects.client_docs') }} ({{ $project->clientAttachments->count() }})</h3>
                                    @foreach ($project->clientAttachments as $attachment)
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('files[]', $attachment->id) !!} <strong>{{ $attachment->filename }}</strong> <em>({{ $attachment->attachmentType->name }})</em>
                                            </label>
                                        </div>
                                    @endforeach

                                    <hr>
                                    <h3>{{ trans('admin.projects.admin_docs') }} ({{ $project->adminAttachments->count() }})</h3>
                                    @foreach ($project->adminAttachments as $attachment)
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('files[]', $attachment->id) !!} <strong>{{ $attachment->filename }}</strong> <em>({{ $attachment->attachmentType->name }})</em>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                                <label for="amount" class="control-label col-md-3">Monto Cotización</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">CLP $</span>
                                        {!! Form::text('amount', '0', ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                @if ($errors->has('amount'))
                                    {!! $errors->first('amount', '<p class="col-md-offset-3 col-md-9 text-error">:message</p>') !!}
                                @endif
                            </div>

                            <hr >
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary">{{ trans('form.send_quote_button') }}</button>
                            </div>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection

@section('inlinescript')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });
    </script>
@endsection
