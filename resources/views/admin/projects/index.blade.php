@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">

            <div class="page-header">
                <h1>{{ trans('admin.projects.title') }}</h1>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('admin.projects.table.client_name') }}</th>
                    <th>{{ trans('admin.projects.table.created_at') }}</th>
                    <th>{{ trans('admin.projects.table.status') }}</th>
                    <th>{{ trans('admin.projects.table.options') }}</th>
                </tr>
                </thead>

                <tbody>
                @if ($projects->total() > 0)
                    @foreach($projects as $project)
                        <tr class="{{  $project->state_color }}">
                            <td>{{ $project->id }}</td>
                            <td>{{ $project->client->name }}</td>
                            <td>{{ $project->created_at->format('d-m-Y H:i') }}</td>
                            <td>
                                {{ !empty($project->status) ? $project->status : '--' }}
                                @if (!empty($project->status))
                                    <sup><small><span class="fa fa-question-circle" data-toggle="tooltip" title="{{ \App\Models\ProjectStates::states($project->status)->name }}"></span></small></sup>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.projects.edit', ['project' => $project->id]) }}" class="btn btn-info btn-sm"><span class="fa fa-pencil"></span></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" class="text-center">
                            <p>{{ trans('admin.projects.table.no_results_msg') }}</p>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>

            {!! $projects !!}
        </div>
    </div>
@endsection
