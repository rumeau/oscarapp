@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">

                    <div class="panel-heading">{{ trans('admin.dashboard') }}</div>

                    <div class="panel-body">
                        {!! trans('admin.welcome_msg') !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
