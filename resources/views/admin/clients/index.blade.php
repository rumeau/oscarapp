@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">

            <div class="page-header">
                <h1>{{ trans('admin.clients.title') }}</h1>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>{{ trans('admin.clients.table.client_name') }}</th>
                    <th>{{ trans('admin.clients.table.email') }}</th>
                    <th>{{ trans('admin.clients.table.contact_name') }}</th>
                    <th>{{ trans('admin.clients.table.created_at') }}</th>
                    <th>{{ trans('admin.clients.table.options') }}</th>
                </tr>
                </thead>

                <tbody>
                @if ($clients->total() > 0)
                    @foreach($clients as $client)
                        <tr>
                            <td>{{ $client->id }}</td>
                            <td><span class="fa {{ $client->client_type === \App\Models\Client::CLIENT_TYPE_PERSON ? 'fa-user' : 'fa-building' }}"></span></td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->email }}</td>
                            <td>{{ $client->client_type === App\Models\Client::CLIENT_TYPE_PERSON ? '' : $client->contact_name }}</td>
                            <td>{{ $client->created_at->format('d-m-Y H:i') }}</td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">
                            <p>{{ trans('admin.clients.table.no_results_msg') }}</p>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>

            {!! $clients !!}
        </div>
    </div>
@endsection
