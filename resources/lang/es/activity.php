<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 19/01/2017
 * Time: 14:12
 */

return [
    'project_activity' => [
        'project_started' => 'Proyecto ingresado en sistema',
        'project_updated' => 'Proyecto actualizado',
        'new_attachment' => 'Se ha cargado un nuevo archivo: :filename',
        'attachment_type_updated' => 'Se ha cambiado el tipo de documento :filename a :attachmentType',
        'attachment_deleted' => 'Se ha eliminado el documento :filename',
        'quote_sent' => 'Se ha enviado una cotizacion - :date',

    ]
];
