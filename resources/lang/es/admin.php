<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 18/01/2017
 * Time: 22:52
 */

return [
    'menu' => [
        'projects' => 'Proyectos',
        'clients' => 'Clientes',
    ],

    'dashboard' => 'Dashboard',
    'welcome_msg' => 'Bienvenido a su panel de administracion',

    'projects' => [
        'title' => 'Projectos',
        'name' => 'Nombre',
        'contact_name' => 'Nombre Contacto',
        'email' => 'E-mail',
        'comment' => 'Comentario',
        'project_documents' => 'Documentos del Proyecto',

        'client_docs' => 'Antecedentes del Cliente',
        'admin_docs' => 'Documentos Agregados',

        'quote' => [
            'title' => 'Prespuesto del proyecto',
            'quote_already_exists' => 'Ya se ha enviado una cotización para este proyecto'
        ],

        'table' => [
            'client_name' => 'Cliente',
            'created_at' => 'Fecha de Ingreso',
            'status' => 'Estado',
            'options' => 'Opciones',
            'no_results_msg' => 'No se han encontrado resultados'
        ],
    ],

    'clients' => [
        'title' => 'Clientes',

        'table' => [
            'type' => 'Tipo de Cliente',
            'client_name' => 'Cliente',
            'contact_name' => 'Contacto',
            'email' => 'Email',
            'created_at' => 'Fecha de Ingreso',
            'options' => 'Opciones',
            'no_results_msg' => 'No se han encontrado resultados'
        ],
    ],
];
