<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 19/01/2017
 * Time: 13:30
 */

return [
    'project_started' => [
        'subject' => 'Su solicitud ha sido ingresada!',
        'greeting' => 'Hola, :name!',
        'line_1' => 'Hemos recibido su solicitud',
        'line_2' => 'Uno de nuestros administradores se pondrá en contacto con usted a la brevedad',
    ],

    'new_project_submitted' => [
        'subject' => 'Nueva solicitud de presupuesto ingresada!',
        'greeting' => 'Hola, :name!',
        'line_1' => 'Un nuevo proyecto ha sido ingresado en la plataforma para presupuesto, a continuacion estan los detalles:',
        'att_1' => 'Nombre: ',
        'att_2' => 'Email: ',
        'att_3' => 'Empresa: ',
        'action_1' => 'Ver en la Plataforma',
        'line_2' => 'El cliente ha sido notificado de que el ingreso ha sido exitoso.',
    ],

    'whoops' => 'Oops',
    'hello' => 'Hola!',
    'regards' => 'Atentamente',
    'problem_link' => 'Si tiene problemas al hacer click en el botón ":actionText", copie y pegue la URL a continuacion en su navegador:'
];
