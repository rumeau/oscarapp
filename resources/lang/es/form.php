<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 18/01/2017
 * Time: 21:43
 */

return [
    'your_name' => 'Su Nombre',
    'your_email' => 'Su E-mail',
    'your_company_name' => 'Nombre de su empresa',
    'provide_your_attachment_msg' => 'Proporcione sus planos en formato DWG para entregarle un servicio mas rápido y eficiente',
    'create_project_submit' => 'Enviar',

    'save_comment_button' => 'Guardar Comentario',
    'send_quote_button' => 'Enviar Presupuesto',
    'send_job_button' => 'Enviar Proyecto',

    'quote' => [
        'subject' => 'Asunto',
        'body' => 'Contenido',
        'select_files' => 'Seleccione el o los archivos a adjuntar',
    ]
];
