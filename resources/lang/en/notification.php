<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 19/01/2017
 * Time: 13:30
 */

return [
    'project_started' => [
        'greeting' => 'Hola!',
        'line_1' => 'Hemos recibido su solicitud',
        'line_2' => 'Uno de nuestros administradores se pondrá en contacto con usted a la brevedad',
    ],

    'regards' => 'Atentamente',
    'problem_link' => 'If you’re having trouble clicking the "{{ $actionText }}" button,
                                                            copy and paste the URL below into your web browser:'
];
